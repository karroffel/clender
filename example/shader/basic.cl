float4 fragment(read_only image2d_t tex, int2 uv)
{
	uint4 icolor = read_imageui(tex, uv);
	float4 color = convert_float4(icolor) / (float4) (255);
	return color;
}

float3 vertex(float3 vertex,
              uint2 screen_dimensions,
              int vertex_pos
              , global float3 *vertex_buffer
              , global float2 *uv_buffer
              , read_only image2d_t diffuse
              )
{
	return vertex;
}



float4 interpolate(float3 bcc,
                   int pos,
                   float2 screen_dimensions,
                   float3 coord
                   , global float3 *vertex_buffer
                   , global float2 *uv_buffer
                   , read_only image2d_t diffuse
                   )
{
	float2 uvs[3];
	
	global float2 *uv_buf = uv_buffer + pos;
	
	uvs[0] = uv_buf[0];
	uvs[1] = uv_buf[1];
	uvs[2] = uv_buf[2];
	
	int2 uv = convert_int2((float2) (bcc.x) * uvs[0] + 
	                       (float2) (bcc.y) * uvs[1] + 
	                       (float2) (bcc.z) * uvs[2]);
	
	return fragment(diffuse, uv);
	
	// return (float4) (1.0);
}
