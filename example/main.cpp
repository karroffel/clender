#include <iostream>

#include <CL/cl.h>

#include <glm/glm.hpp>

#include <vector>

#include <cstdint>



#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


#include <clender.h>
#include <util.h>



#define IMAGE_PATH "./output_images/z_buffer_test.png"

#define WOOD_TEXTURE "./example/textures/wood.png"



int main(int argc, char **argv)
{
	int width  = 1920;
	int height = 1080;
	
	std::vector<cl_char> buffer(width * height * 4);

	int twidth;
	int theight;
	int tn;
	auto wood_tex = stbi_load(WOOD_TEXTURE, &twidth, &theight, &tn, 0);
	
	/*
	// rectangle
	std::vector<cl_float3> vertices = {
		{{-1.0,        -1.0,     1}},
		{{-1.0,         1.0,     2}},
		{{ 1.0,        -1.0,     3}},
		{{ 1.0,        -1.0,    -2}},
		{{ 1.0,         1.0,     4}},
		{{-1.0,         1.0,     8}}
	};
	*/
	
	/* 
	std::vector<cl_float4> colors = {
		{{0.0, 1.0, 1.0, 1.0}},
		{{0.0, 0.0, 1.0, 1.0}},
		{{1.0, 0.0, 1.0, 1.0}},
		
		{{1.0, 0.0, 1.0, 1.0}},
		{{1.0, 1.0, 1.0, 1.0}},
		{{0.0, 0.0, 1.0, 1.0}},
	};
	*/
	
	std::vector<cl_float3> vertices = {
		{{-1.0,        1.0 / 5,     1}},
		{{1.0,         -1.0,        2}},
		{{1.0 / 5,     1.0,         3}},
		{{-1.0,        1.0 / 2,     -2}},
		{{1.0 / 2,     -1.0,        4}},
		{{1.0,         1.0 / 1.25,  8}}
	};
	
	std::vector<cl_float2> uv = {
		{{0,                 0}},
		{{0,                 (cl_float) theight}},
		{{(cl_float) twidth, 0}},
		{{(cl_float) twidth, 0}},
		{{(cl_float) twidth, (cl_float) theight}},
		{{0,                 (cl_float) theight}}
	};
	

	
	Clender clender(width, height);
	
	
	clender.create_attr_buffer(0,
	                           vertices.size() * sizeof(cl_float3),
	                           "float3");
	clender.create_attr_buffer(1,
	                           uv.size() * sizeof(cl_int2),
	                           "float2");
	
	cl_image_format fmt;
	fmt.image_channel_data_type = CL_UNSIGNED_INT8;
	fmt.image_channel_order     = CL_RGBA;
	clender.create_texture(0, twidth, theight, fmt, CL_MEM_READ_ONLY);
	
	auto shader_code = read_file("./example/shader/basic.cl");
	clender.compile_with_shader(shader_code);
	
	clender.clear_color();
	clender.clear_zbuffer();
	
	clender.attr_data(0,
	                  vertices.data(),
	                  vertices.size() * sizeof(cl_float3));
	clender.attr_data(1,
	                  uv.data(),
	                  uv.size() * sizeof(cl_int2));
	clender.write_texture(0, wood_tex);
	
	
	clender.draw(2);
	
	
	clender.read_color_buffer(buffer.data());
	
	stbi_write_png(IMAGE_PATH, width, height, 4, buffer.data(), 0);
	
	std::cout << "successfully wrote image to file" << std::endl;
	
	stbi_image_free(wood_tex);
	
	return 0;
	
}
