# clender

Software renderer written in C++ and OpenCL

Dependencies
------------

 * clang++, because llvm rocks
 * scons
 * glm
 * OpenCL, duh


Building
--------

`scons` to build.
