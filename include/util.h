#ifndef UTIL_H
#define UTIL_H

#include <string>

std::string read_file(std::string filepath);

#endif
