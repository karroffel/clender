#ifndef CLENDER_H
#define CLENDER_H


#include <CL/cl.h>

#include <glm/glm.hpp>

#include <string>
#include <vector>
#include <tuple>

class Clender
{
public:
	Clender(int width, int height);
	~Clender();
	
	void clear_color();
	void clear_zbuffer();
	
	void compile_with_shader(std::string filepath);
	
	void create_attr_buffer(int id, size_t size, std::string type = "char");
	void attr_data(int id, void *data, size_t size, size_t offset = 0);
	
	
	void create_texture(int id,
	                    int width,
	                    int height,
	                    cl_image_format fmt,
	                    cl_mem_flags flags);
	void write_texture(int id, const void *buffer);
	
	cl_int draw(int num = 1, int start = 0);
	
	void read_color_buffer(cl_char *buffer) const;
	void read_z_buffer(cl_float *buffer) const;


private:
	int m_width;
	int m_height;
	
	cl_platform_id   m_platform;
	cl_device_id     m_device;
	cl_context       m_context;
	cl_command_queue m_cqueue;
	
	cl_program m_program;
	cl_kernel  m_vertex_kernel;
	cl_kernel  m_rasterize_kernel;
	
	cl_mem m_color_buffer;
	cl_mem m_z_buffer;
	
	cl_mem m_vert_tmp_buffer;
	
	// (buffer, len, type)
	std::vector<std::tuple<cl_mem, size_t, std::string>> m_attrs;
	
	// TODO: add create, write and read functions
	std::vector<std::pair<cl_mem, cl_mem_flags>> m_textures;
	
	
	
	cl_int _add_dyn_kernel_args(cl_kernel kernel, size_t arg_pos);
	
	cl_int _draw_single(int start);
	
	cl_int _rasterize(int pos);
	std::pair<std::string, std::string> _prep_base_code();

};







#endif // CLENDER_H

