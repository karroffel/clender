#include <clender.h>
#include <util.h>

#include <cstring>

#include <iostream>

#include <map>

#include <CL/cl.h>


static std::string print_kernel_exec_err(cl_int err);
static std::string print_kernel_create_err(cl_int err);
static std::string print_image_read_err(cl_int err);

static std::string flags_to_modifier(cl_mem_flags flags);


static cl_int read_image(cl_command_queue command_queue,
                         cl_mem img,
                         void *buffer);

static cl_mem create_image(cl_context context,
                           cl_image_format format,
                           int width,
                           int height,
                           cl_mem_flags flags,
                           cl_int *err);

static void write_image(cl_command_queue cqueue, cl_mem img, void *contents);


/*
 * The constructor sets up the OpenCL context and creates the needed buffers.
 */
Clender::Clender(int width, int height)
{
	
	m_width       = width;
	m_height      = height;
	
	cl_int err;
	err = clGetPlatformIDs(1, &m_platform, nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not receive OpenCL platform" << std::endl;
		return;
	}
	
	
	err = clGetDeviceIDs(m_platform,
	                     CL_DEVICE_TYPE_ALL,
	                     1,
	                     &m_device,
	                     nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not get OpenCL device" << std::endl;
		return;
	}
	
	
	m_context = clCreateContext(nullptr,
	                            1,
	                            &m_device,
	                            nullptr,
	                            nullptr,
	                            &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create OpenCL context" << std::endl;
		return;
	}
	
	
	m_cqueue = clCreateCommandQueue(m_context, m_device, 0, &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create command queue" << std::endl;
		return;
	}
	
	const auto F = CL_MEM_READ_WRITE;
	
	cl_image_format format;
	format.image_channel_order     = CL_RGBA;
	format.image_channel_data_type = CL_UNSIGNED_INT8;
	
	m_color_buffer = create_image(m_context, format, width, height, F,&err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create color buffer" << std::endl;
		return;
	}
	
	format.image_channel_order     = CL_INTENSITY;
	format.image_channel_data_type = CL_FLOAT;
	m_z_buffer = create_image(m_context, format, width, height, F, &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create z buffer" << std::endl;
		return;
	}
	
	
	m_vert_tmp_buffer = clCreateBuffer(m_context,
	                                   CL_MEM_WRITE_ONLY,
	                                   sizeof(cl_int3) * 3,
	                                   nullptr,
	                                   &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create vertex tmp buffer" << std::endl;
		return;
	}
}

/*
 * Destructor releases all the OpenCL resources
 */
Clender::~Clender()
{
	for (auto tex : m_textures) {
		clReleaseMemObject(tex.first);
	}
	m_textures.clear();
	
	for (auto attr : m_attrs) {
		clReleaseMemObject(std::get<0>(attr));
	}
	m_attrs.clear();
	
	clReleaseKernel(m_rasterize_kernel);
	clReleaseKernel(m_vertex_kernel);
	clReleaseProgram(m_program);
	clReleaseMemObject(m_vert_tmp_buffer);
	clReleaseMemObject(m_z_buffer);
	clReleaseMemObject(m_color_buffer);
	clReleaseCommandQueue(m_cqueue);
	clReleaseContext(m_context);
	clReleaseDevice(m_device);
}

/*
 * Clear the content of the color buffer.
 *
 * After allocating a buffer the content is undefined.
 * To avoid undefined behaviour and to reuse the same buffers you need to clear
 * the contents of it.
 */
void Clender::clear_color()
{
	size_t origin[3] = {0, 0, 0};
	size_t region[3] = {(size_t) m_width, (size_t) m_height, 1};
	
	cl_int4 color = {{0, 0, 0, 0}};
	
	
	cl_int err;
	err = clEnqueueFillImage(m_cqueue,
	                         m_color_buffer,
	                         &color,
	                         origin,
	                         region,
	                         0,
	                         nullptr,
	                         nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not clear color buffer" << std::endl;
	}
}

/*
 * Clear the content of the z buffer.
 *
 * After allocating a buffer the content is undefined.
 * To avoid undefined behaviour and to reuse the same buffers you need to clear
 * the contents of it.
 */
void Clender::clear_zbuffer()
{
	size_t origin[3] = {0, 0, 0};
	size_t region[3] = {(size_t) m_width, (size_t) m_height, 1};
	
	cl_float4 color = {{-CL_INFINITY, 0, 0, 1}};
	
	
	cl_int err;
	err = clEnqueueFillImage(m_cqueue,
	                         m_z_buffer,
	                         &color,
	                         origin,
	                         region,
	                         0,
	                         nullptr,
	                         nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not clear color buffer" << std::endl;
	}
}



#define PT_PLACEHOLDER "__TYPE_PARAMS"
#define  P_PLACEHOLDER "__PARAMS"

/*
 * Add a shader file to the OpenCL program.
 *
 * The shader file must contain the vertex and the fragment shader functions.
 *
 * TODO: create documentation? Right now the shaders change a lot concerning
 *       function parameters. So maybe wait a little.
 *
 *       Investigate if there is a nicer way to include shaders than to build
 *       the program.
 */
void Clender::compile_with_shader(std::string shader_code)
{
	cl_int err;
	
	std::string cpp_sources[2] = {read_file("./kernel/clender.cl"),
	                              shader_code};
	char const *sources[2] = {cpp_sources[1].c_str(),
	                          cpp_sources[0].c_str()};
	size_t length[2] = {cpp_sources[1].length(),
	                    cpp_sources[0].length()};
	
	m_program = clCreateProgramWithSource(m_context,
	                                      2,
	                                      sources,
	                                      length,
	                                      &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create program" << std::endl;
		return;
	}
	
	auto defs = _prep_base_code();
	std::string options = "-Werror";
	options.append(" -D")
	       .append(PT_PLACEHOLDER)
	       .append("=")
	       .append(defs.first);
       
	options.append(" -D")
	       .append(P_PLACEHOLDER)
	       .append("=")
	       .append(defs.second);
	
	const char *opts = options.c_str();
	
	err = clBuildProgram(m_program, 1, &m_device, opts, nullptr, nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not build program" << std::endl;
		
		size_t len;
		clGetProgramBuildInfo(m_program,
		                      m_device,
		                      CL_PROGRAM_BUILD_LOG,
		                      0,
		                      nullptr,
		                      &len);
		
		char *buffer = new char[len];
		clGetProgramBuildInfo(m_program,
		                      m_device,
		                      CL_PROGRAM_BUILD_LOG,
		                      len,
		                      buffer,
		                      nullptr);
		
		std::cerr << buffer << std::endl;
		
		delete[] buffer;
		
		return;
	}
	
	
	m_vertex_kernel = clCreateKernel(m_program, "vertex_stage", &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create kernel.\n"
		          << print_kernel_create_err(err) << std::endl;
		return;
	}
	
	m_rasterize_kernel = clCreateKernel(m_program, "rasterize", &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create kernel.\n"
		          << print_kernel_create_err(err) << std::endl;
		return;
	}
	
}

/*
 * Adds the attribute buffers and textures to the kernel params
 */
std::pair<std::string, std::string> Clender::_prep_base_code()
{
	// build type signatures
	std::string type_sig = "";
	// first attributes
	
	for (int i = 0; i < m_attrs.size(); i++) {
		auto buf = m_attrs[i];
		type_sig.append(", global ")
		        .append(std::get<2>(buf))
		        .append(" *attr")
		        .append(std::to_string(i));
	}
	
	// then textures
	for (int i = 0; i < m_textures.size(); i++) {
		auto tex = m_textures[i];
		type_sig.append(", ")
		        .append(flags_to_modifier(tex.second))
		        .append(" image2d_t texture")
		        .append(std::to_string(i));
	}
	
	
	// build arguments
	std::string args = "";
	// first attributes
	for (int i = 0; i < m_attrs.size(); i++) {
		auto buf = m_attrs[i];
		args.append(", ")
		    .append(" attr")
		    .append(std::to_string(i));
	}
	
	// then textures
	for (int i = 0; i < m_textures.size(); i++) {
		args.append(", texture")
		    .append(std::to_string(i));
	}

	std::string signature;
	signature.append("\"")
	         .append(type_sig)
                 .append("\"");
	
	std::string params;
	params.append("\"")
	      .append(args)
              .append("\"");
	
	return std::make_pair(signature, params);
}


/*
 * Call the rasterizer
 */
cl_int Clender::_rasterize(int pos)
{
	glm::ivec2 min(0, 0);
	glm::ivec2 max(0, 0);
	
	
	cl_int3 vert[3];
	cl_int err;
	
	err = clEnqueueReadBuffer(m_cqueue,
	                          m_vert_tmp_buffer,
	                          CL_TRUE,
	                          0,
	                          sizeof(cl_int3) * 3,
	                          vert,
	                          0,
	                          nullptr,
	                          nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not read vertices" << std::endl;
		return err;
	}
	
	glm::ivec3 vert0(vert[0].s[0], vert[0].s[1], vert[0].s[2]);
	glm::ivec3 vert1(vert[1].s[0], vert[1].s[1], vert[1].s[2]);
	glm::ivec3 vert2(vert[2].s[0], vert[2].s[1], vert[2].s[2]);
	
	glm::ivec3 verts[3] = {vert0, vert1, vert2};
	
	for (int vi = 0; vi < 3; vi++) {
		glm::ivec3 vert = verts[vi];
		
		for (int i = 0; i < 2; i++) {
			if (vert[i] < min[i]) {
				min[i] = vert[i];
			} else if (vert[i] > max[i]) {
				max[i] = vert[i];
			}
		}
	}
	
	auto diff = max - min;
	
	int width  = diff.x;
	int height = diff.y;
	
	cl_uint2 screen_dims = {{(uint) m_width, (uint) m_height}};
	
	cl_int3 cverts[3] = { {{vert0.x, vert0.y, vert0.z}},
	                      {{vert1.x, vert1.y, vert1.z}},
	                      {{vert2.x, vert2.y, vert2.z}} };
	
	
	const cl_kernel kernel = m_rasterize_kernel;
	
	err  = clSetKernelArg(kernel, 0, sizeof(cl_mem),   &m_color_buffer);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem),   &m_z_buffer);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_uint2), &screen_dims);
	err |= clSetKernelArg(kernel, 3, sizeof(cl_mem),   &m_vert_tmp_buffer);
	err |= clSetKernelArg(kernel, 4, sizeof(cl_int),   &pos);
	err |= _add_dyn_kernel_args(kernel, 5);
	if (err != CL_SUCCESS) {
		std::cerr << "could not set kernel arg" << std::endl;
		return err;
	}
	
	size_t global_offset[2];
	global_offset[0] = glm::max(min.x, 0);
	global_offset[1] = glm::max(min.y, 0);
	
	size_t work_size[2];
	work_size[0] = glm::min(width, m_width);
	work_size[1] = glm::min(height, m_height);
	
	err = clEnqueueNDRangeKernel(m_cqueue,
	                             kernel,
	                             2,
	                             global_offset,
	                             work_size,
	                             nullptr,
	                             0,
	                             nullptr,
	                             nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not queue kernel task.\n"
		          << print_kernel_exec_err(err) << std::endl;
		return err;
	}
	
	return CL_SUCCESS;
}


/*
 * Register a buffer so its size and position can be used to set up the buffers
 *     on the GPU
 */
void Clender::create_attr_buffer(int id, size_t size, std::string type)
{
	if (id != m_attrs.size()) {
		std::cerr << "Cannot create buffers out of order" << std::endl;
		return;
	}
	
	m_attrs.resize(id + 1);
	
	cl_mem mem;
	cl_int err;
	
	mem = clCreateBuffer(m_context,
	                     CL_MEM_READ_ONLY,
	                     size,
	                     nullptr,
	                     &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create attribute buffer "
		          << id << std::endl;
		return;
	}
	
	m_attrs[id] = std::make_tuple(mem, size, type);
}


/*
 * Set the contents of a buffer on the GPU
 * 
 * IMPORTANT: `create_buffers` must be called before this function can be used
 */
void Clender::attr_data(int id, void *data, size_t size, size_t offset)
{
	cl_int err;
	err = clEnqueueWriteBuffer(m_cqueue,
	                           std::get<0>(m_attrs[id]),
	                           CL_TRUE,
	                           offset,
	                           size,
	                           data,
	                           0,
	                           nullptr,
	                           nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not write data to buffer"
		          << id << std::endl;
		return;
	}
}






/*
 * Register a buffer so its size and position can be used to set up the buffers
 *     on the GPU
 */
void Clender::create_texture(int id,
                             int width,
                             int height,
                             cl_image_format fmt,
                             cl_mem_flags flags)
{
	if (id != m_textures.size()) {
		std::cerr << "Cannot create textures out of order" << std::endl;
		return;
	}
	
	m_textures.resize(id + 1);
	
	cl_mem mem;
	cl_int err;
	
	mem = create_image(m_context, fmt, width, height, flags, &err);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not create texture "
		          << id << std::endl;
		return;
	}
	
	m_textures[id].first = mem;
	m_textures[id].second = flags;
}

/*
 * Writes data to the content of a texture
 */
void Clender::write_texture(int id, const void *buffer)
{
	size_t width;
	size_t height;
	
	clGetImageInfo(m_textures[id].first,
	              CL_IMAGE_WIDTH,
	              sizeof(size_t),
	              &width,
	              nullptr);
	clGetImageInfo(m_textures[id].first,
	              CL_IMAGE_HEIGHT,
	              sizeof(size_t),
	              &height,
	              nullptr);
	
	size_t origin[3] = {0, 0, 0};
	size_t region[3] = {width, height, 1};
	
	cl_int err;
	err = clEnqueueWriteImage(m_cqueue,
	                          m_textures[id].first,
	                          CL_TRUE,
	                          origin,
	                          region,
	                          0,
	                          0,
	                          buffer,
	                          0,
	                          nullptr,
	                          nullptr);
	if (err != CL_SUCCESS) {
		std::cout << "Could not write to image " << id << std::endl;
	}
}



/*
 * Draw `num` triangles starting at pos `start` in the vertex buffer
 */
cl_int Clender::draw(int num, int start)
{
	cl_int err = CL_SUCCESS;
	while (num > 0) {
		err = _draw_single(start);
		start += 3;
		if (err != CL_SUCCESS) {
			std::cerr << "Could not draw triangle" << std::endl;
			return err;
		}
		num--;
	}
	return err;
}


/*
 * Draw a single triangle. Vertices are located at `vertex_buffer[start]`
 */
cl_int Clender::_draw_single(int start)
{
	
	cl_kernel kernel = m_vertex_kernel;
	cl_int err;
	
	cl_uint2 screen_dims = {{(uint) m_width, (uint) m_height}};

	
	err  = clSetKernelArg(kernel, 0, sizeof(cl_int),  &start);
	err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &m_vert_tmp_buffer);
	err |= clSetKernelArg(kernel, 2, sizeof(cl_int2), &screen_dims);
	err |= _add_dyn_kernel_args(kernel, 3);
	
	if (err != CL_SUCCESS) {
		std::cerr << "Could not set kernel arg" << std::endl;
		return !CL_SUCCESS;
	}
	
	size_t work_size = 3;
	err = clEnqueueNDRangeKernel(m_cqueue,
	                             m_vertex_kernel,
	                             1,
	                             nullptr,
	                             &work_size,
	                             nullptr,
	                             0,
	                             nullptr,
	                             nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not queue kernel task.\n"
		          << print_kernel_exec_err(err) << std::endl;
		return err;
	}
	
	return _rasterize(start);
}

/*
 * Adds attribute buffers and textures as kernel arguments
 */
cl_int Clender::_add_dyn_kernel_args(cl_kernel kernel, size_t arg_pos)
{
	cl_int err = CL_SUCCESS;
	
	for (auto attr : m_attrs) {
		err |= clSetKernelArg(kernel,
		                      arg_pos,
		                      sizeof(cl_mem),
		                      &std::get<0>(attr));
		arg_pos++;
	}
	
	for (auto &text : m_textures) {
		err |= clSetKernelArg(kernel,
		                      arg_pos,
		                      sizeof(cl_mem),
		                      &text.first);
		arg_pos++;
	}
	
	return err;
}


/*
 * Read the color buffer from GPU memory to host memory
 *
 * buffer must point to allocated memory
 */
void Clender::read_color_buffer(cl_char* buffer) const
{
	cl_int err = read_image(m_cqueue, m_color_buffer, buffer);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not read color buffer\n\t"
		          << print_image_read_err(err) << std::endl;
	}
}

/*
 * Read the z buffer from GPU memory to host memory
 *
 * buffer must point to allocated memory
 */
void Clender::read_z_buffer(cl_float* buffer) const
{
	cl_int err = read_image(m_cqueue, m_color_buffer, buffer);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not read z buffer\n"
		          << print_image_read_err(err) << std::endl;
	}
}


/*
 * Helper function to create image buffers
 */
cl_mem create_image(cl_context context,
                    cl_image_format format,
                    int width,
                    int height,
                    cl_mem_flags flags,
                    cl_int *err)
{
	
	
	cl_image_desc desc;
	desc.image_type        = CL_MEM_OBJECT_IMAGE2D;
	desc.image_width       = width;
	desc.image_height      = height;
	desc.image_row_pitch   = 0;
	desc.image_slice_pitch = 0;
	desc.num_mip_levels    = 0;
	desc.num_samples       = 0;
	
	cl_mem image;
	image = clCreateImage(context,
	                      flags,
	                      &format,
	                      &desc,
	                      nullptr,
	                      err);
	return image;
}


/*
 * Helper function to read data from image buffers on the GPU to host memory
 *
 * buffer must point to allocated memory
 */
cl_int read_image(cl_command_queue command_queue,
                 cl_mem img,
                 void *buffer)
{
	size_t width;
	size_t height;
	
	cl_int err;
	
	clGetImageInfo(img, CL_IMAGE_WIDTH, sizeof(size_t), &width, nullptr);
	clGetImageInfo(img, CL_IMAGE_HEIGHT, sizeof(size_t), &height, nullptr);
	
	size_t origin[3] = {0, 0, 0};
	size_t region[3] = {width, height, 1};
	
	err = clEnqueueReadImage(command_queue,
	                         img,
	                         CL_TRUE,
	                         origin,
	                         region,
	                         0,
	                         0,
	                         buffer,
	                         0,
	                         nullptr,
	                         nullptr);
	
	return err;
	
}

/*
 * Writes data to an OpenCL image
 */
static void write_image(cl_command_queue cqueue, cl_mem img, void *contents)
{
	size_t width;
	size_t height;
	
	cl_int err;
	
	clGetImageInfo(img, CL_IMAGE_WIDTH, sizeof(size_t), &width, nullptr);
	clGetImageInfo(img, CL_IMAGE_HEIGHT, sizeof(size_t), &height, nullptr);
	
	size_t origin[3] = {0, 0, 0};
	size_t region[3] = {width, height, 1};
	
	err = clEnqueueWriteImage(cqueue,
	                          img,
	                          CL_TRUE,
	                          origin,
	                          region,
	                          0,
	                          0,
	                          contents,
	                          0,
	                          nullptr,
	                          nullptr);
	if (err != CL_SUCCESS) {
		std::cerr << "Could not write image" << std::endl;
	}
}




#define ERRMAP_ENTRY(x) {x, #x}

/*
 * Helper function to get the name of an execution error
 */
static std::string print_kernel_exec_err(cl_int err)
{
	static std::map<cl_int, std::string> error_names = {
		ERRMAP_ENTRY(CL_INVALID_PROGRAM_EXECUTABLE),
		ERRMAP_ENTRY(CL_INVALID_COMMAND_QUEUE),
		ERRMAP_ENTRY(CL_INVALID_KERNEL),
		ERRMAP_ENTRY(CL_INVALID_CONTEXT),
		ERRMAP_ENTRY(CL_INVALID_KERNEL_ARGS),
		ERRMAP_ENTRY(CL_INVALID_WORK_DIMENSION),
		ERRMAP_ENTRY(CL_INVALID_GLOBAL_WORK_SIZE),
		ERRMAP_ENTRY(CL_INVALID_GLOBAL_OFFSET),
		ERRMAP_ENTRY(CL_INVALID_WORK_GROUP_SIZE),
		ERRMAP_ENTRY(CL_INVALID_WORK_ITEM_SIZE),
		ERRMAP_ENTRY(CL_MISALIGNED_SUB_BUFFER_OFFSET),
		ERRMAP_ENTRY(CL_INVALID_IMAGE_SIZE),
		ERRMAP_ENTRY(CL_MEM_OBJECT_ALLOCATION_FAILURE),
		ERRMAP_ENTRY(CL_INVALID_EVENT_WAIT_LIST),
		ERRMAP_ENTRY(CL_OUT_OF_RESOURCES),
		ERRMAP_ENTRY(CL_OUT_OF_HOST_MEMORY)
	};
	return error_names[err];
}


/*
 * Helper function to get the name of an kernel creation error
 */
static std::string print_kernel_create_err(cl_int err)
{
	static std::map<cl_int, std::string> error_names = {
		ERRMAP_ENTRY(CL_INVALID_PROGRAM),
		ERRMAP_ENTRY(CL_INVALID_PROGRAM_EXECUTABLE),
		ERRMAP_ENTRY(CL_INVALID_KERNEL_NAME),
		ERRMAP_ENTRY(CL_INVALID_KERNEL_DEFINITION),
		ERRMAP_ENTRY(CL_INVALID_VALUE),
		ERRMAP_ENTRY(CL_OUT_OF_RESOURCES),
		ERRMAP_ENTRY(CL_OUT_OF_HOST_MEMORY)
	};
	return error_names[err];
}


/*
 * Helper function to get the name of an image read error
 */
static std::string print_image_read_err(cl_int err)
{
	static std::map<cl_int, std::string> error_names = {
		ERRMAP_ENTRY(CL_INVALID_COMMAND_QUEUE),
		ERRMAP_ENTRY(CL_INVALID_CONTEXT),
		ERRMAP_ENTRY(CL_INVALID_MEM_OBJECT),
		ERRMAP_ENTRY(CL_INVALID_VALUE),
		ERRMAP_ENTRY(CL_INVALID_EVENT_WAIT_LIST),
		ERRMAP_ENTRY(CL_INVALID_IMAGE_SIZE),
		// ERRMAP_ENTRY(CL_INVALID_IMAGE_FORMAT),
		ERRMAP_ENTRY(CL_MEM_OBJECT_ALLOCATION_FAILURE),
		ERRMAP_ENTRY(CL_INVALID_OPERATION),
		ERRMAP_ENTRY(CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST),
		ERRMAP_ENTRY(CL_OUT_OF_RESOURCES),
		ERRMAP_ENTRY(CL_OUT_OF_HOST_MEMORY)
	};
	return error_names[err];
}


/*
 * Helper function to get the correct modifier for cl_mem_flags
 */
static std::string flags_to_modifier(cl_mem_flags flags)
{
	switch (flags) {
	case CL_MEM_READ_ONLY:
		return "read_only";
	case CL_MEM_READ_WRITE:
		return "read_write";
	case CL_MEM_WRITE_ONLY:
		return "write_only";
	}
	return "\"invalid mem flags\"";
}
