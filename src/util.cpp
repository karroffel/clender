#include <util.h>

#include <fstream>

/*
 * Helper function to read the contents of a file to a string
 */
std::string read_file(std::string filepath)
{
	std::ifstream t(filepath);
	std::string str;

	t.seekg(0, std::ios::end);
	str.reserve(t.tellg());
	t.seekg(0, std::ios::beg);

	str.assign((std::istreambuf_iterator<char>(t)),
	           std::istreambuf_iterator<char>());
	return str;
}
