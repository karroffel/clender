/*
 * Calculate the barycentric coordinates of point p
 */
float3 barycentric(int3 p, int3 pts0, int3 pts1, int3 pts2)
{
	int3 x_vec = (int3) (pts2.x-pts0.x, pts1.x-pts0.x, pts0.x-p.x);
	int3 y_vec = (int3) (pts2.y-pts0.y, pts1.y-pts0.y, pts0.y-p.y);

	float3 f_x_vec = convert_float3(x_vec);
	float3 f_y_vec = convert_float3(y_vec);
	
	float3 u     = cross(f_x_vec, f_y_vec); 
	
	return (float3) (1.0 - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z);
}


/*
 * transform world coordinates to screen coordinates
 */
int3 world_to_screen(float3 coord, uint2 screen_dimensions)
{
	int3 coords;
	coords.z = (int) coord.z;
	coords.x = (int)(((coord.x + 1.0) / 2.0) * (float) screen_dimensions.x);
	coords.y = (int)(((coord.y + 1.0) / 2.0) * (float) screen_dimensions.y);
	return coords;
}


/*
 * Calls the vertex shader
 */
kernel void vertex_stage(int vertex_pos,
                         global int3 *out_vertices,
                         uint2 screen_dimensions
                         __TYPE_PARAMS)
{
	int id = get_global_id(0);
	
	global float3 *vertex_buffer = attr0;
	
	float3 vert = vertex_buffer[vertex_pos + id];
	
	
	float3 new_vert = vertex(vert,
	                         screen_dimensions,
	                         vertex_pos
	                         __PARAMS);
	
	out_vertices[id] = world_to_screen(new_vert, screen_dimensions);
}

/*
 * Rasterize a triangle give by vert0, vert1 and vert2
 */
kernel void rasterize(read_write image2d_t color_buffer,
                      read_write image2d_t z_buffer,
                      uint2 screen_dimensions,
                      global int3 *vertices,
                      int pos
                      __TYPE_PARAMS)
{	
	
	int x = get_global_id(0);
	int y = get_global_id(1);
	
	int2 coord = (int2) (x, y);
	
	

	float3 bcc = barycentric((int3) (coord, 0),
	                         vertices[0],
	                         vertices[1],
	                         vertices[2]);
	
	
	float2 f_screen_dimensions = convert_float2(screen_dimensions);
	float2 f_coord             = convert_float2(coord);
	
	if (bcc.x >= 0 && bcc.y >= 0 && bcc.z >= 0) {
		
		// calculate the z value for the current coordinate
		float z = (float) vertices[0].z * bcc.x +
		          (float) vertices[1].z * bcc.y +
		          (float) vertices[2].z * bcc.z;
		
		float4 z_val = read_imagef(z_buffer, coord);
		
		
		if (z_val.x > z) {
			return;
		}
		
		
		write_imagef(z_buffer, coord, (float4) (z, 0.0, 0.0, 1.0));
		
		float4 f_color = interpolate(bcc,
		                             pos,
		                             f_screen_dimensions,
	                                     (float3)(f_coord, z)
		                             __PARAMS)
		                  * (float4) (255.0);

		uint4 color = convert_uint4(f_color);
		
		write_imageui(color_buffer, coord, color);
	}
}





